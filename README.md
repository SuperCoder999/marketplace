## Installation

- Clone the repo
- Add sdk directory to settings:

  ```bash
  cd android
  echo "sdk.dir=/absolute/path/to/your/Android/sdk\n" > local.properties
  ```

## Start

- `npm run android`

## Docs & tips

- I've added login and registration forms (and a logout button)
- Search and password inputs' icons are clickable :)
- react-navigation v6 (which I've used here) doesn't have a Switch Navigator, so RootNavigator is totally correct.
- Click product's details button to edit/delete it if you're the seller see full info/call seller if you aren't.
- react-native-image-picker is used to select one (avatar) or many (product) pictures
- React Native's Linking API (not an external library) is used to make a phone call
- You should see image carousel (product details) and image picker (registration and product editing/creation)
- Product list page has infinite scroll. To see it, create more than 40 products or go to `src/constants/pagination.constants.ts` and change the `PRODUCTS_PER_PAGE` constant to 3 or 5
