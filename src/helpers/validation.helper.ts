import validator from "validator";

export function validateExists(value: string): string | null {
	return value.length > 0 ? null : "Must not be empty";
}

export function validateEmail(value: string): string | null {
	return validator.isEmail(value) ? null : "Must be a valid email";
}

export function validatePassword(value: string): string | null {
	return value.length >= 8 ? null : "Must be at least 8 characters long";
}

export function validatePhoneNumber(value: string): string | null {
	return validator.isMobilePhone(value)
		? null
		: "Must be a valid phone number";
}

export function validateStringifiedNumber(value: string): string | null {
	return /^-?[0-9]+(,[0-9]+)?$/.test(value) ? null : "Must be a valid number";
}
