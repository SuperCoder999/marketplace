import { Linking } from "react-native";
import { Asset, launchImageLibrary } from "react-native-image-picker";

export async function selectPhoto(): Promise<Asset | null> {
	const { assets } = await launchImageLibrary({
		mediaType: "photo",
	});

	if (assets && assets.length > 0) {
		const newAsset = assets[0];
		return newAsset;
	}

	return null;
}

export async function selectMultiplePhotos(): Promise<Asset[]> {
	const { assets } = await launchImageLibrary({
		mediaType: "photo",
		selectionLimit: 0,
	});

	return assets ?? [];
}

export function makeCall(phoneNumber: string): Promise<void> {
	return Linking.openURL(`tel:${phoneNumber}`);
}
