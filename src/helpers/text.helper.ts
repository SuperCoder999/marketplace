export function addTextEllipsis(text: string, maxWords: number = 15): string {
	const words = text.split(" ");

	if (words.length <= maxWords) {
		return text;
	}

	const limitedText = words.slice(0, maxWords).join(" ");
	return limitedText + "...";
}
