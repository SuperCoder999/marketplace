import Toast from "react-native-simple-toast";

export function notify(message: string): void {
	Toast.showWithGravity(message, Toast.LONG, Toast.BOTTOM);
}

export function notifyError(message: string): void {
	Toast.showWithGravity(`Error: ${message}`, Toast.LONG, Toast.BOTTOM);
}
