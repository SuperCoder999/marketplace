import AsyncStorage from "@react-native-async-storage/async-storage";
import { STORAGE_NAMESPACE } from "../constants/storage.constants";
import StorageKey from "../enums/storage-key.enum";

export function get(key: StorageKey): Promise<string | null> {
	return AsyncStorage.getItem(getKey(key));
}

export function set(key: StorageKey, data: string): Promise<void> {
	return AsyncStorage.setItem(getKey(key), data);
}

export function remove(key: StorageKey): Promise<void> {
	return AsyncStorage.removeItem(getKey(key));
}

function getKey(key: StorageKey): string {
	return `${STORAGE_NAMESPACE}${key}`;
}
