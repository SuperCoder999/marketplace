import { Asset } from "react-native-image-picker";
import { FormFile } from "../typing/file.types";

export function assetToFormFile(asset: Asset): FormFile {
	return {
		uri: asset.uri ?? asset.base64 ?? "",
		name: asset.fileName ?? "",
		type: asset.type ?? "",
	};
}
