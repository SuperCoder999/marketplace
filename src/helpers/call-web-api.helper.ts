import {
	API_BASE_URL,
	DEFAULT_ATTACHMENT_FIELD,
} from "../constants/api.constants";

import HttpMethod from "../enums/http-method.enum";
import StorageKey from "../enums/storage-key.enum";
import { RequestArgs, BodyInit } from "../typing/http.types";
import { assetToFormFile } from "./file.helper";
import { notifyError } from "./notification.helper";
import { get } from "./storage.helper";

export default async function callWebApi(args: RequestArgs): Promise<Response> {
	try {
		const response = await fetch(getUrl(args), await getInit(args));
		throwIfReponseFailed(response);

		return response;
	} catch (error: any) {
		if (args.alertError !== false) {
			notifyError(error.message);
		}

		throw error;
	}
}

function getUrl(args: RequestArgs): string {
	return `${API_BASE_URL}${args.endpont}${stringifyQuery(args.query)}`;
}

async function getInit(args: RequestArgs): Promise<RequestInit> {
	let body: BodyInit;
	let contentType: string = "text/plain";

	const token = await get(StorageKey.UserToken);
	const headers: Record<string, string> = args.headers ?? {};

	if (args.attachment) {
		const form = new FormData();

		form.append(
			args.attachmentFieldName ?? DEFAULT_ATTACHMENT_FIELD,
			assetToFormFile(args.attachment)
		);

		if (args.body) {
			Object.entries(args.body).forEach(([key, value]) =>
				form.append(key, value)
			);
		}

		body = form;
		contentType = "multipart/form-data";
	} else if (args.body) {
		body = JSON.stringify(args.body);
		contentType = "application/json";
	}

	headers["Content-Type"] = contentType;

	if (token) {
		headers.Authorization = `Bearer ${token}`;
	}

	return {
		method: args.method ?? HttpMethod.Get,
		headers,
		...(body ? { body } : {}),
	};
}

function stringifyQuery(query?: Record<string, string | number>): string {
	if (!query) {
		return "";
	}

	const keyString = Object.entries(query)
		.map(([key, value]) => `${key}=${value}`)
		.join("&");

	const separator = "?";

	return `${separator}${keyString}`;
}

function throwIfReponseFailed(response: Response): void {
	if (!response.ok) {
		throw new Error(response.statusText);
	}
}
