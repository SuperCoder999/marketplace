import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Icon from "../../components/Icon";
import ProductListNavigator from "../ProductListNavigator";
import ModifyProductPage from "../../pages/ModifyProductPage";

const Tabs = createBottomTabNavigator();

const routeDisplayNames: Record<string, string> = {
	List: "Products",
	Edit: "Add product",
};

const routeIconNames: Record<string, string> = {
	List: "list-circle",
	Edit: "add-circle",
};

const MarketNavigator: React.FC = () => {
	return (
		<Tabs.Navigator
			screenOptions={({ route }) => ({
				title: routeDisplayNames[route.name],
				tabBarIcon: ({ focused }) => {
					let icon = routeIconNames[route.name];

					if (!focused) {
						icon += "-outline";
					}

					return <Icon name={icon} />;
				},
			})}
		>
			<Tabs.Screen name="List" component={ProductListNavigator} />
			<Tabs.Screen name="Edit" component={ModifyProductPage} />
		</Tabs.Navigator>
	);
};

export default MarketNavigator;
