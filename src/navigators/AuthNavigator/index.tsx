import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LoginPage from "../../pages/LoginPage";
import RegisterPage from "../../pages/RegisterPage";

const Stack = createNativeStackNavigator();

const AuthNavigator: React.FC = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen name="Login" component={LoginPage} />
			<Stack.Screen name="Register" component={RegisterPage} />
		</Stack.Navigator>
	);
};

export default AuthNavigator;
