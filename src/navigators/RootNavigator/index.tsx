import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import RootState from "../../redux/state";
import { usePromiseDispatch } from "../../redux/store";
import { loadUser } from "../../redux/auth/actions";
import Preloader from "../../components/Preloader";
import AuthNavigator from "../AuthNavigator";
import MarketNavigator from "../MarketNavigator";

const RootNavigator: React.FC = () => {
	const dispatch = usePromiseDispatch();

	const { user, userLoaded, loading } = useSelector(
		(state: RootState) => state.auth
	);

	useEffect(() => {
		if (!userLoaded) {
			dispatch(loadUser());
		}
	}, [dispatch, userLoaded]);

	if (loading) {
		return <Preloader />;
	}

	if (!user) {
		return <AuthNavigator />;
	}

	return <MarketNavigator />;
};

export default RootNavigator;
