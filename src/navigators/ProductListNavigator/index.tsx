import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ProductListPage from "../../pages/ProductListPage";
import ExpandedProductPage from "../../pages/ExpandedProductPage";

const Stack = createNativeStackNavigator();

const ProductListNavigator: React.FC = () => {
	return (
		<Stack.Navigator screenOptions={() => ({ header: () => null })}>
			<Stack.Screen name="ListAll" component={ProductListPage} />
			<Stack.Screen name="ListExpanded" component={ExpandedProductPage} />
		</Stack.Navigator>
	);
};

export default ProductListNavigator;
