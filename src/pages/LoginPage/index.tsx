import React from "react";
import Page from "../../components/Page";
import Login from "../../containers/Auth/Login";

const LoginPage: React.FC = () => {
	return (
		<Page>
			<Login />
		</Page>
	);
};

export default LoginPage;
