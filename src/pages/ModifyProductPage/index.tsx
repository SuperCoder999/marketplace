import React from "react";
import ModifyProduct from "../../containers/ModifyProduct";
import Page from "../../components/Page";
import useRemoveExpandedProduct from "../../hooks/remove-expanded-product.hook";

const ModifyProductPage: React.FC = () => {
	useRemoveExpandedProduct();

	return (
		<Page>
			<ModifyProduct />
		</Page>
	);
};

export default ModifyProductPage;
