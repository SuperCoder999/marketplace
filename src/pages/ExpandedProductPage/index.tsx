import React from "react";
import Page from "../../components/Page";
import FullProduct from "../../containers/FullProduct";
import useRemoveExpandedProduct from "../../hooks/remove-expanded-product.hook";

const ExpandedProductPage: React.FC = () => {
	useRemoveExpandedProduct();

	return (
		<Page>
			<FullProduct />
		</Page>
	);
};

export default ExpandedProductPage;
