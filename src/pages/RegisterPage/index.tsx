import React from "react";
import Page from "../../components/Page";
import Register from "../../containers/Auth/Register";

const RegisterPage: React.FC = () => {
	return (
		<Page>
			<Register />
		</Page>
	);
};

export default RegisterPage;
