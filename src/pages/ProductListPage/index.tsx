import React from "react";
import Page from "../../components/Page";
import ProductList from "../../containers/ProductList";

const ProductListPage: React.FC = () => {
	return (
		<Page>
			<ProductList />
		</Page>
	);
};

export default ProductListPage;
