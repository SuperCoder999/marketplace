import ProductListDto from "../typing/dto/product/product-list.dto";
import ProductSavedDto from "../typing/dto/product/product-saved.dto";

export function productSavedDtoToProductListDto(
	dto: ProductSavedDto
): ProductListDto {
	const { id, title, description, price, pictures } = dto;

	return {
		id,
		title,
		description,
		price,
		picture: pictures.length > 0 ? pictures[0].url : undefined,
	};
}
