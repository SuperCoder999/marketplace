import { Asset } from "react-native-image-picker";
import HttpMethod from "../enums/http-method.enum";
import callWebApi from "../helpers/call-web-api.helper";
import PictureDto from "../typing/dto/picture/picture.dto";

export async function addUserAvatar(avatar: Asset): Promise<string | null> {
	try {
		const response = await callWebApi({
			endpont: "Users/avatar",
			method: HttpMethod.Post,
			attachment: avatar,
		});

		return response.text();
	} catch (err) {
		return null;
	}
}

export async function addProductPicture(
	productId: number,
	picture: Asset
): Promise<PictureDto | null> {
	try {
		const response = await callWebApi({
			endpont: `Products/${productId}/Pictures`,
			method: HttpMethod.Post,
			attachment: picture,
		});

		return (await response.json()) as PictureDto;
	} catch (err) {
		return null;
	}
}

export async function deleteProductPicture(
	productId: number,
	id: number
): Promise<void> {
	try {
		await callWebApi({
			endpont: `Products/${productId}/Pictures/${id}`,
			method: HttpMethod.Delete,
		});
	} catch (err) {
		//
	}
}
