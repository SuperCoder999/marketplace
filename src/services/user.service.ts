import { Asset } from "react-native-image-picker";
import HttpMethod from "../enums/http-method.enum";
import StorageKey from "../enums/storage-key.enum";
import callWebApi from "../helpers/call-web-api.helper";
import { remove, set } from "../helpers/storage.helper";
import AuthorizedUserDto from "../typing/dto/user/authorized-user.dto";
import UserCredentialsDto from "../typing/dto/user/user-credentials.dto";
import UserRegisterDto from "../typing/dto/user/user-register.dto";
import UserDto from "../typing/dto/user/user.dto";
import { addUserAvatar } from "./picture.service";

async function authenticateUser(body: UserCredentialsDto) {
	const loginResponse = await callWebApi({
		endpont: "Auth/login",
		method: HttpMethod.Post,
		body,
	});

	const { token } = (await loginResponse.json()) as AuthorizedUserDto;
	await set(StorageKey.UserToken, token);
}

export async function loginUser(
	body: UserCredentialsDto
): Promise<UserDto | null> {
	try {
		await authenticateUser(body);
		return getUser();
	} catch {
		return null;
	}
}

export async function getUser(): Promise<UserDto | null> {
	try {
		const response = await callWebApi({
			endpont: "Users/details",
			alertError: false,
		});

		return (await response.json()) as UserDto;
	} catch {
		await remove(StorageKey.UserToken);
		return null;
	}
}

export async function registerUser(
	body: UserRegisterDto,
	avatar: Asset | null
): Promise<UserDto | null> {
	try {
		const response = await callWebApi({
			endpont: "Auth/register",
			method: HttpMethod.Post,
			body,
		});

		await authenticateUser({ email: body.email, password: body.password });
		const user = (await response.json()) as UserDto;

		if (avatar) {
			const avatarLink = await addUserAvatar(avatar);
			user.avatar = avatarLink || undefined;
		}

		return user;
	} catch {
		return null;
	}
}

export async function logoutUser(): Promise<void> {
	await remove(StorageKey.UserToken);
}
