import { Asset } from "react-native-image-picker";
import { PRODUCTS_PER_PAGE } from "../constants/pagination.constants";
import HttpMethod from "../enums/http-method.enum";
import callWebApi from "../helpers/call-web-api.helper";
import PictureDto from "../typing/dto/picture/picture.dto";
import EditProductDto from "../typing/dto/product/edit-product.dto";
import ProductFilterDto from "../typing/dto/product/product-filter.dto";
import ProductListDto from "../typing/dto/product/product-list.dto";
import ProductSavedDto from "../typing/dto/product/product-saved.dto";
import ProductDto from "../typing/dto/product/product.dto";
import { addProductPicture, deleteProductPicture } from "./picture.service";

export async function getProducts(
	filter: ProductFilterDto
): Promise<ProductListDto[]> {
	try {
		const response = await callWebApi({
			endpont: "Products",
			query: {
				...filter,
				perPage: PRODUCTS_PER_PAGE,
			},
		});

		return (await response.json()) as ProductListDto[];
	} catch {
		return [];
	}
}

export async function getProduct(id: number): Promise<ProductDto | null> {
	try {
		const response = await callWebApi({ endpont: `Products/${id}` });
		return (await response.json()) as ProductDto;
	} catch (err) {
		return null;
	}
}

export async function postProduct(
	body: EditProductDto,
	pictures: Asset[]
): Promise<ProductSavedDto | null> {
	try {
		const response = await callWebApi({
			endpont: "Products",
			method: HttpMethod.Post,
			body,
		});

		const product = (await response.json()) as ProductSavedDto;

		const createdPictures = await Promise.all(
			pictures.map((p) => addProductPicture(product.id, p))
		);

		return {
			...product,
			pictures: createdPictures.filter(Boolean) as PictureDto[],
		};
	} catch {
		return null;
	}
}

export async function putProduct(
	id: number,
	body: EditProductDto,
	newPictures: Asset[],
	deletedPictureIds: number[]
): Promise<ProductSavedDto | null> {
	try {
		const response = await callWebApi({
			endpont: `Products/${id}`,
			method: HttpMethod.Put,
			body,
		});

		const product = (await response.json()) as ProductSavedDto;

		const createdPictures = await Promise.all(
			newPictures.map((p) => addProductPicture(product.id, p))
		);

		await Promise.all(
			deletedPictureIds.map((p) => deleteProductPicture(product.id, p))
		);

		const pictures = [...product.pictures];

		deletedPictureIds.forEach((p) => {
			const index = product.pictures.findIndex(
				(suspectPicture) => suspectPicture.id === p
			);

			if (index >= 0) {
				pictures.splice(index, 1);
			}
		});

		return {
			...product,
			pictures: [
				...pictures,
				...(createdPictures.filter(Boolean) as PictureDto[]),
			],
		};
	} catch {
		return null;
	}
}

export async function removeProduct(id: number): Promise<void> {
	try {
		await callWebApi({
			endpont: `Products/${id}`,
			method: HttpMethod.Delete,
		});
	} catch {
		//
	}
}
