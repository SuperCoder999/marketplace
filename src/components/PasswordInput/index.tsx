import React, { useState } from "react";
import Input from "../Input";

interface Props {
	value?: string;
	defaultValue?: string;
	placeholder?: string;
	onChange?: (text: string) => void;
	validate?: (value: string) => string | null;
}

const PasswordInput: React.FC<Props> = ({
	value,
	defaultValue,
	placeholder,
	onChange,
	validate,
}) => {
	const [hidden, setHidden] = useState<boolean>(true);
	const icon = hidden ? "eye" : "eye-off";

	const toggleHidden = () => setHidden(!hidden);

	return (
		<Input
			value={value}
			defaultValue={defaultValue}
			placeholder={placeholder}
			icon={icon}
			secure={hidden}
			onChange={onChange}
			onIconPress={toggleHidden}
			validate={validate}
		/>
	);
};

export default PasswordInput;
