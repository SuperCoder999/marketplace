import React, { useEffect, useState } from "react";
import { KeyboardTypeOptions, Text, TextInput, View } from "react-native";
import Icon from "../Icon";
import styles from "./styles";

interface Props {
	value?: string;
	defaultValue?: string;
	placeholder?: string;
	textarea?: boolean;
	icon?: string;
	keyboard?: KeyboardTypeOptions;
	secure?: boolean;
	onChange?: (text: string) => void;
	onIconPress?: () => void;
	validate?: (value: string) => string | null;
}

const Input: React.FC<Props> = ({
	value,
	defaultValue,
	placeholder,
	textarea,
	icon,
	keyboard,
	secure,
	onChange,
	onIconPress,
	validate,
}) => {
	const [val, setVal] = useState<string>(value ?? defaultValue ?? "");
	const [errorMessage, setErrorMessage] = useState<string | null>(null);

	useEffect(() => {
		const newValue = value ?? defaultValue ?? "";

		if (newValue !== val) {
			setVal(val);
		}
	}, [value, defaultValue]);

	const change = (value: string) => {
		if (validate) {
			const newError = validate(value);

			if (newError !== errorMessage) {
				setErrorMessage(newError);
			}
		}

		if (onChange) {
			onChange(value);
		}

		setVal(value);
	};

	return (
		<View style={styles.container}>
			<View style={styles.field}>
				<TextInput
					style={styles.input}
					value={val}
					defaultValue={defaultValue}
					placeholder={placeholder}
					keyboardType={keyboard}
					secureTextEntry={secure}
					numberOfLines={textarea ? 4 : 1}
					onChangeText={change}
				/>
				{icon ? <Icon name={icon} onPress={onIconPress} /> : null}
			</View>
			{errorMessage ? (
				<Text style={styles.error}>{errorMessage}</Text>
			) : null}
		</View>
	);
};

export default Input;
