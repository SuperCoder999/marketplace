import { StyleSheet } from "react-native";
import Color from "../../enums/color.enum";

const styles = StyleSheet.create({
	container: {
		width: "100%",
		marginBottom: 15,
	},
	field: {
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
		borderBottomColor: Color.GreyDark,
		borderBottomWidth: 2,
	},
	input: {
		flexGrow: 1,
	},
	error: {
		color: Color.Error,
		marginTop: 5,
	},
});

export default styles;
