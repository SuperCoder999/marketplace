import { StyleSheet } from "react-native";
import Color from "../../enums/color.enum";

const styles = StyleSheet.create({
	page: {
		flexGrow: 1,
		backgroundColor: Color.White,
	},
});

export default styles;
