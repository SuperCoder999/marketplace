import React from "react";
import { View } from "react-native";
import styles from "./styles";

const Page: React.FC = ({ children }) => {
	return <View style={styles.page}>{children}</View>;
};

export default Page;
