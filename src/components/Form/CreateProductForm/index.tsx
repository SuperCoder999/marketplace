import React, { useState } from "react";
import { Asset } from "react-native-image-picker";
import { Button, View } from "react-native";
import EditProductDto from "../../../typing/dto/product/edit-product.dto";
import Input from "../../Input";
import MultipleImagePicker from "../../ImagePicker/Multiple";
import Color from "../../../enums/color.enum";

import {
	validateExists,
	validateStringifiedNumber,
} from "../../../helpers/validation.helper";

import styles from "../styles";

interface Props {
	onSubmit: (data: EditProductDto, pictures: Asset[]) => void;
}

const CreateProductForm: React.FC<Props> = ({ onSubmit }) => {
	const [title, setTitle] = useState<string>("");
	const [priceText, setPriceText] = useState<string>("");
	const [description, setDescription] = useState<string>("");
	const [pictures, setPictures] = useState<Asset[]>([]);

	const invalid = Boolean(
		validateExists(title) || validateStringifiedNumber(priceText)
	);

	const submit = () => {
		if (invalid) {
			return;
		}

		onSubmit({ title, price: Number(priceText), description }, pictures);
	};

	return (
		<View style={styles.form}>
			<Input
				value={title}
				placeholder="Title"
				onChange={setTitle}
				validate={validateExists}
			/>
			<Input
				value={description}
				placeholder="Description"
				textarea
				onChange={setDescription}
			/>
			<Input
				value={priceText}
				placeholder="Price"
				keyboard="numeric"
				onChange={setPriceText}
				validate={validateStringifiedNumber}
			/>
			<MultipleImagePicker onPicked={setPictures} />
			<Button
				title="Create product"
				disabled={invalid}
				color={Color.Primary}
				onPress={submit}
			/>
		</View>
	);
};

export default CreateProductForm;
