import React, { useMemo, useState } from "react";
import { Button, View } from "react-native";
import { Asset } from "react-native-image-picker";
import Color from "../../../enums/color.enum";

import {
	validateEmail,
	validatePassword,
	validatePhoneNumber,
	validateExists,
} from "../../../helpers/validation.helper";

import UserRegisterDto from "../../../typing/dto/user/user-register.dto";
import SingleImagePicker from "../../ImagePicker/Single";
import Input from "../../Input";
import PasswordInput from "../../PasswordInput";
import styles from "../styles";

interface Props {
	onSubmit: (data: UserRegisterDto, avatar: Asset | null) => void;
}

const RegisterForm: React.FC<Props> = ({ onSubmit }) => {
	const [name, setName] = useState<string>("");
	const [email, setEmail] = useState<string>("");
	const [phoneNumber, setPhoneNumber] = useState<string>("");
	const [password, setPassword] = useState<string>("");
	const [avatar, setAvatar] = useState<Asset | null>(null);

	const invalid = useMemo(
		() =>
			Boolean(
				validateExists(name) ||
					validateEmail(email) ||
					validatePhoneNumber(phoneNumber) ||
					validatePassword(password)
			),
		[email, password]
	);

	const submit = () => {
		if (invalid) {
			return;
		}

		onSubmit({ name, email, phoneNumber, password }, avatar);
	};

	return (
		<View style={styles.form}>
			<Input
				value={name}
				placeholder="Name"
				icon="person"
				onChange={setName}
				validate={validateExists}
			/>
			<Input
				value={email}
				placeholder="Email"
				icon="at-circle"
				onChange={setEmail}
				validate={validateEmail}
			/>
			<Input
				value={phoneNumber}
				placeholder="Phone number"
				icon="call"
				keyboard="phone-pad"
				onChange={setPhoneNumber}
				validate={validatePhoneNumber}
			/>
			<PasswordInput
				value={password}
				placeholder="Password"
				onChange={setPassword}
				validate={validatePassword}
			/>
			<SingleImagePicker fieldName="avatar" onPicked={setAvatar} />
			<Button
				title="Register"
				disabled={invalid}
				color={Color.Primary}
				onPress={submit}
			/>
		</View>
	);
};

export default RegisterForm;
