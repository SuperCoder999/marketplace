import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	form: {
		width: "100%",
	},
	buttons: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
});

export default styles;
