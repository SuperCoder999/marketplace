import React, { useEffect, useState } from "react";
import { Asset } from "react-native-image-picker";
import { Button, View } from "react-native";
import EditProductDto from "../../../typing/dto/product/edit-product.dto";
import ProductDto from "../../../typing/dto/product/product.dto";
import PictureDto from "../../../typing/dto/picture/picture.dto";
import Input from "../../Input";
import MultipleImagePicker from "../../ImagePicker/Multiple";
import Color from "../../../enums/color.enum";

import {
	validateExists,
	validateStringifiedNumber,
} from "../../../helpers/validation.helper";

import styles from "../styles";

interface Props {
	defaultProduct: ProductDto;
	onSubmit: (
		data: EditProductDto,
		pictures: Asset[],
		deletedPictureIds: number[]
	) => void;
	onSubmitDelete: () => void;
}

const EditProductForm: React.FC<Props> = ({
	defaultProduct,
	onSubmit,
	onSubmitDelete,
}) => {
	const [title, setTitle] = useState<string>(defaultProduct.title);

	const [priceText, setPriceText] = useState<string>(
		String(defaultProduct.price)
	);

	const [description, setDescription] = useState<string>(
		defaultProduct.description
	);

	const [pictures, setPictures] = useState<Asset[]>([]);
	const [existingPictures, setExistingPictures] = useState<PictureDto[]>([]);
	const [deletedPictures, setDeletedPictures] = useState<number[]>([]);

	const invalid = Boolean(
		validateExists(title) || validateStringifiedNumber(priceText)
	);

	useEffect(() => {
		setTitle(defaultProduct.title);
		setPriceText(String(defaultProduct.price));
		setDescription(defaultProduct.description);
		setExistingPictures(defaultProduct.pictures);
		setDeletedPictures([]);
	}, [defaultProduct]);

	const removeExistingPicture = (id: number) => {
		setExistingPictures(existingPictures.filter((p) => p.id !== id));
		setDeletedPictures([...deletedPictures, id]);
	};

	const submit = () => {
		if (invalid) {
			return;
		}

		onSubmit(
			{ title, price: Number(priceText), description },
			pictures,
			deletedPictures
		);
	};

	return (
		<View style={styles.form}>
			<Input
				value={title}
				placeholder="Title"
				onChange={setTitle}
				validate={validateExists}
			/>
			<Input
				value={description}
				placeholder="Description"
				textarea
				onChange={setDescription}
			/>
			<Input
				value={priceText}
				placeholder="Price"
				keyboard="numeric"
				onChange={setPriceText}
				validate={validateStringifiedNumber}
			/>
			<MultipleImagePicker
				existing={existingPictures}
				onPicked={setPictures}
				onDeletedExisting={removeExistingPicture}
			/>
			<View style={styles.buttons}>
				<Button
					title="Edit product"
					disabled={invalid}
					color={Color.Primary}
					onPress={submit}
				/>
				<Button
					title="Delete product"
					color={Color.GreyDark}
					onPress={onSubmitDelete}
				/>
			</View>
		</View>
	);
};

export default EditProductForm;
