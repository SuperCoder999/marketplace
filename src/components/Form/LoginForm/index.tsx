import React, { useMemo, useState } from "react";
import { Button, View } from "react-native";
import validator from "validator";
import Color from "../../../enums/color.enum";

import {
	validateEmail,
	validateExists,
} from "../../../helpers/validation.helper";

import UserCredentialsDto from "../../../typing/dto/user/user-credentials.dto";
import Input from "../../Input";
import PasswordInput from "../../PasswordInput";
import styles from "../styles";

interface Props {
	onSubmit: (data: UserCredentialsDto) => void;
}

const LoginForm: React.FC<Props> = ({ onSubmit }) => {
	const [email, setEmail] = useState<string>("");
	const [password, setPassword] = useState<string>("");

	const invalid = useMemo(
		() => Boolean(validateEmail(email) || validateExists(password)),
		[email, password]
	);

	const submit = () => {
		if (invalid) {
			return;
		}

		onSubmit({ email, password });
	};

	return (
		<View style={styles.form}>
			<Input
				value={email}
				placeholder="Email"
				icon="at-circle"
				onChange={setEmail}
				validate={validateEmail}
			/>
			<PasswordInput
				value={password}
				placeholder="Password"
				onChange={setPassword}
				validate={validateExists}
			/>
			<Button
				title="Login"
				disabled={invalid}
				color={Color.Primary}
				onPress={submit}
			/>
		</View>
	);
};

export default LoginForm;
