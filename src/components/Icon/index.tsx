import React from "react";
import { TouchableOpacity } from "react-native";
import Ionicon from "react-native-vector-icons/Ionicons";

interface Props {
	name: string;
	size?: number;
	onPress?: () => void;
	onLongPress?: () => void;
}

const Icon: React.FC<Props> = ({ name, size = 24, onPress, onLongPress }) => {
	const ionicon = <Ionicon name={name} size={size} />;

	if (!onPress && !onLongPress) {
		return ionicon;
	}

	return (
		<TouchableOpacity onPress={onPress} onLongPress={onLongPress}>
			{ionicon}
		</TouchableOpacity>
	);
};

export default Icon;
