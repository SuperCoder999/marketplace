import React, { useMemo, useState } from "react";
import { Button, Image, Text, TouchableOpacity, View } from "react-native";
import { Asset } from "react-native-image-picker";
import Color from "../../../enums/color.enum";
import { selectMultiplePhotos } from "../../../helpers/native.helper";
import PictureDto from "../../../typing/dto/picture/picture.dto";
import Icon from "../../Icon";
import styles from "../styles";

interface Props {
	fieldName?: string;
	existing?: PictureDto[];
	onPicked: (images: Asset[]) => void;
	onDeletedExisting?: (id: number) => void;
}

const MultipleImagePicker: React.FC<Props> = ({
	fieldName = "images",
	existing,
	onPicked,
	onDeletedExisting,
}) => {
	const [assets, setAssets] = useState<Asset[]>([]);

	const assetUris = useMemo(
		() =>
			assets
				.map((asset) => asset?.uri ?? asset?.base64 ?? null)
				.filter(Boolean) as string[],
		[assets]
	);

	const pick = async () => {
		const images = await selectMultiplePhotos();
		const newAssets = [...assets, ...images];

		onPicked(newAssets);
		setAssets(newAssets);
	};

	const deleteNew = (index: number) => {
		const newAssets = [...assets];
		newAssets.splice(index, 1);

		onPicked(newAssets);
		setAssets(newAssets);
	};

	const deleteExisting = (id: number) => {
		if (onDeletedExisting) {
			onDeletedExisting(id);
		}
	};

	return (
		<View style={styles.picker}>
			<View style={styles.images}>
				{existing
					? existing.map(({ id, url: uri }) => (
							<View style={styles.imageWrapper} key={id}>
								<Image style={styles.image} source={{ uri }} />
								{onDeletedExisting ? (
									<TouchableOpacity
										style={styles.deleteIcon}
										onPress={() => deleteExisting(id)}
									>
										<Icon name="trash" size={18} />
									</TouchableOpacity>
								) : null}
							</View>
					  ))
					: null}
				{assetUris.length
					? assetUris.map((uri, i) => (
							<View style={styles.imageWrapper} key={i}>
								<Image style={styles.image} source={{ uri }} />
								<TouchableOpacity
									style={styles.deleteIcon}
									onPress={() => deleteNew(i)}
								>
									<Icon name="trash" size={18} />
								</TouchableOpacity>
							</View>
					  ))
					: null}
			</View>
			<View style={styles.button}>
				<Button
					title={`Pick ${fieldName}`}
					color={Color.Secondary}
					onPress={pick}
				/>
			</View>
		</View>
	);
};

export default MultipleImagePicker;
