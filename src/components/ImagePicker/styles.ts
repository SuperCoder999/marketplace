import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	picker: {
		width: "100%",
		alignItems: "center",
		marginBottom: 15,
	},
	images: {
		flexDirection: "row",
		flexWrap: "wrap",
	},
	imageWrapper: {
		width: 60,
		height: 60,
		marginRight: 10,
		marginBottom: 10,
		position: "relative",
	},
	image: {
		width: 60,
		height: 60,
	},
	deleteIcon: {
		position: "absolute",
		top: -9,
		right: -9,
	},
	button: {
		width: "100%",
	},
});

export default styles;
