import React, { useMemo, useState } from "react";
import { Button, Image, Text, View } from "react-native";
import { Asset } from "react-native-image-picker";
import Color from "../../../enums/color.enum";
import { selectPhoto } from "../../../helpers/native.helper";
import styles from "../styles";

interface Props {
	fieldName?: string;
	onPicked: (image: Asset | null) => void;
}

const SingleImagePicker: React.FC<Props> = ({
	fieldName = "image",
	onPicked,
}) => {
	const [asset, setAsset] = useState<Asset | null>(null);

	const assetUri = useMemo(
		() => asset?.uri ?? asset?.base64 ?? null,
		[asset]
	);

	const pick = async () => {
		const image = await selectPhoto();

		onPicked(image);
		setAsset(image);
	};

	return (
		<View style={styles.picker}>
			<View style={styles.imageWrapper}>
				{assetUri ? (
					<Image style={styles.image} source={{ uri: assetUri }} />
				) : asset ? (
					<Text style={styles.image}>Image selected</Text>
				) : null}
			</View>
			<View style={styles.button}>
				<Button
					title={`Pick ${fieldName}`}
					color={Color.Secondary}
					onPress={pick}
				/>
			</View>
		</View>
	);
};

export default SingleImagePicker;
