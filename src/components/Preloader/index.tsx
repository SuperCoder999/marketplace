import React, { useEffect, useRef } from "react";
import { View, Animated, Easing } from "react-native";
import styles from "./styles";

const Preloader: React.FC = () => {
	const rotation = useRef<Animated.Value>(new Animated.Value(0)).current;

	useEffect(() => {
		Animated.loop(
			Animated.timing(rotation, {
				toValue: 1,
				duration: 500,
				easing: Easing.linear,
				useNativeDriver: true,
			})
		).start();
	}, [rotation]);

	const degrees = rotation.interpolate({
		inputRange: [0, 1],
		outputRange: ["0deg", "360deg"],
	});

	const transform = [{ rotateZ: degrees }];

	return (
		<View style={styles.container}>
			<Animated.View style={[styles.spinner, { transform }]} />
		</View>
	);
};

export default Preloader;
