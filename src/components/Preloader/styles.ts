import { StyleSheet } from "react-native";
import Color from "../../enums/color.enum";

const styles = StyleSheet.create({
	container: {
		backgroundColor: Color.Modal,
		position: "absolute",
		top: 0,
		left: 0,
		width: "100%",
		height: "100%",
		alignItems: "center",
		justifyContent: "center",
		zIndex: 10,
	},
	spinner: {
		width: 50,
		height: 50,
		borderWidth: 10,
		borderColor: Color.GreyLight,
		borderTopColor: Color.GreyDark,
		borderRadius: 25,
	},
});

export default styles;
