import { StyleSheet } from "react-native";
import Color from "../../enums/color.enum";

const styles = StyleSheet.create({
	container: {
		padding: 15,
		marginBottom: 10,
		borderColor: Color.GreyDark,
		borderWidth: 1,
		flexDirection: "row",
	},
	picture: {
		width: 90,
		height: 90,
	},
	texts: {
		paddingLeft: 10,
		flexShrink: 1,
	},
	header: {
		flexDirection: "row",
		justifyContent: "space-between",
		fontWeight: "bold",
		width: "100%",
	},
	black: {
		color: Color.Black,
	},
});

export default styles;
