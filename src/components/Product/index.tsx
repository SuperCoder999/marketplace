import React, { useMemo } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import { DEFAULT_PICTURE } from "../../constants/picture.constants";
import { addTextEllipsis } from "../../helpers/text.helper";
import ProductListDto from "../../typing/dto/product/product-list.dto";
import styles from "./styles";

interface Props {
	product: ProductListDto;
	onExpand?: (id: number) => void;
}

const Product: React.FC<Props> = ({
	product: { id, title, description, price, picture },
	onExpand,
}) => {
	const trimmedDescription = useMemo(
		() => addTextEllipsis(description),
		[description]
	);

	return (
		<View style={styles.container}>
			<Image
				style={styles.picture}
				source={{ uri: picture ?? DEFAULT_PICTURE }}
			/>
			<View style={styles.texts}>
				<View style={styles.header}>
					<Text style={styles.black}>{title}</Text>
					<Text style={styles.black}>{price}$</Text>
				</View>
				<Text>{trimmedDescription}</Text>
				<TouchableOpacity onPress={() => onExpand && onExpand(id)}>
					<Text style={styles.black}>Details</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

export default Product;
