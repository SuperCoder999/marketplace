import React, { useEffect, useState } from "react";
import { Image, TouchableOpacity, View } from "react-native";
import Icon from "../Icon";
import styles from "./styles";

interface Props {
	urls: string[];
}

const ImageCarousel: React.FC<Props> = ({ urls }) => {
	const [currentIndex, setCurrentIndex] = useState<number>(0);

	useEffect(() => {
		if (urls.length <= currentIndex) {
			setCurrentIndex(Math.max(0, urls.length - 1));
		}
	}, [urls.length]);

	const next = () => {
		setCurrentIndex((currentIndex + 1) % urls.length);
	};

	const back = () => {
		setCurrentIndex((currentIndex + urls.length - 1) % urls.length);
	};

	if (urls.length === 0) {
		return null;
	}

	return (
		<View style={styles.container}>
			{urls.length > 1 ? (
				<TouchableOpacity style={styles.arrowWrapper} onPress={next}>
					<Icon name="chevron-back" size={64} />
				</TouchableOpacity>
			) : null}
			<Image source={{ uri: urls[currentIndex] }} style={styles.image} />
			{urls.length > 1 ? (
				<TouchableOpacity style={styles.arrowWrapper} onPress={back}>
					<Icon name="chevron-forward" size={64} />
				</TouchableOpacity>
			) : null}
		</View>
	);
};

export default ImageCarousel;
