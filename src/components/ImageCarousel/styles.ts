import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	container: {
		flexGrow: 1,
		width: "100%",
		flexDirection: "row",
		alignItems: "center",
		marginBottom: 15,
	},
	arrowWrapper: {
		justifyContent: "center",
		alignItems: "center",
		width: 64,
	},
	image: {
		flexGrow: 1,
		minHeight: 200,
		height: "100%",
	},
});

export default styles;
