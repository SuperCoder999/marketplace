import { useEffect } from "react";
import { useNavigation } from "@react-navigation/native";
import { usePromiseDispatch } from "../redux/store";
import { removeExpandedProduct } from "../redux/products/actions";

export default function useRemoveExpandedProduct() {
	const dispatch = usePromiseDispatch();
	const navigation = useNavigation();

	useEffect(() => {
		return () => void dispatch(removeExpandedProduct());
	}, [dispatch, navigation]);
}
