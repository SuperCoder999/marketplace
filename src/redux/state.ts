import AuthState from "./auth/state";
import ProductsState from "./products/state";

export default interface RootState {
	auth: AuthState;
	products: ProductsState;
}
