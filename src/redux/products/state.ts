import ProductFilterDto from "../../typing/dto/product/product-filter.dto";
import ProductListDto from "../../typing/dto/product/product-list.dto";
import ProductDto from "../../typing/dto/product/product.dto";

export default interface ProductsState {
	products: ProductListDto[];
	productsPagination: ProductFilterDto;
	productsLoaded: boolean;
	expandedProduct: ProductDto | null;
	expandedProductLoaded: boolean;
	hasMoreProducts: boolean;
	loading: boolean;
}

export const initialState: ProductsState = {
	products: [],
	productsPagination: {
		page: 1,
		filter: "",
	},
	productsLoaded: false,
	expandedProduct: null,
	expandedProductLoaded: false,
	hasMoreProducts: true,
	loading: false,
};
