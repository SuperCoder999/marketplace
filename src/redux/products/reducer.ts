import { createReducer, isAnyOf } from "@reduxjs/toolkit";
import { productSavedDtoToProductListDto } from "../../mappers/product.mapper";

import {
	createProduct,
	deleteProduct,
	loadExpandedProduct,
	loadMoreProducts,
	loadProducts,
	removeExpandedProduct,
	updateProduct,
} from "./actions";

import { initialState } from "./state";

const productsReducer = createReducer(initialState, (builder) => {
	builder.addCase(loadProducts.pending, (state) => ({
		...state,
		productsLoaded: false,
		hasMoreProducts: true,
		loading: true,
	}));

	builder.addCase(
		loadProducts.fulfilled,
		(state, { payload: { filter, products } }) => ({
			...state,
			products,
			productsPagination: filter,
			loading: false,
		})
	);

	builder.addCase(
		loadMoreProducts.fulfilled,
		(state, { payload: products }) => ({
			...state,
			products: [...state.products, ...products],
			productsPagination: {
				...state.productsPagination,
				page:
					state.productsPagination.page +
					(products.length > 0 ? 1 : 0),
			},
			hasMoreProducts: products.length > 0,
			loading: false,
		})
	);

	builder.addCase(loadExpandedProduct.pending, (state) => ({
		...state,
		expandedProductLoaded: false,
		loading: true,
	}));

	builder.addCase(
		loadExpandedProduct.fulfilled,
		(state, { payload: product }) => ({
			...state,
			expandedProduct: product,
			expandedProductLoaded: true,
			loading: false,
		})
	);

	builder.addCase(removeExpandedProduct, (state) => ({
		...state,
		expandedProduct: null,
		expandedProductLoaded: false,
	}));

	builder.addCase(createProduct.fulfilled, (state, { payload: product }) => {
		if (!product) {
			return state;
		}

		return {
			...state,
			loading: false,
			products: [
				...state.products,
				productSavedDtoToProductListDto(product),
			],
		};
	});

	builder.addCase(updateProduct.fulfilled, (state, { payload: product }) => {
		if (!product) {
			return state;
		}

		const index = state.products.findIndex((p) => p.id === product.id);
		const newProducts = [...state.products];

		if (index >= 0) {
			newProducts[index] = productSavedDtoToProductListDto(product);
		}

		let newExpandedProduct = state.expandedProduct;

		if (newExpandedProduct?.id === product.id) {
			newExpandedProduct = {
				...newExpandedProduct,
				...product,
			};
		}

		return {
			...state,
			loading: false,
			products: newProducts,
			expandedProduct: newExpandedProduct,
		};
	});

	builder.addCase(deleteProduct.fulfilled, (state, { payload: id }) => {
		if (!id) {
			return state;
		}

		const index = state.products.findIndex((p) => p.id === id);
		const newProducts = [...state.products];

		if (index >= 0) {
			newProducts.splice(index, 1);
		}

		let newExpandedProduct = state.expandedProduct;

		if (newExpandedProduct?.id === id) {
			newExpandedProduct = null;
		}

		return {
			...state,
			loading: false,
			products: newProducts,
			expandedProduct: newExpandedProduct,
			expandedProductLoaded: Boolean(newExpandedProduct),
		};
	});

	builder.addMatcher(
		isAnyOf(
			loadMoreProducts.pending,
			createProduct.pending,
			updateProduct.pending,
			deleteProduct.pending
		),
		(state) => ({
			...state,
			loading: true,
		})
	);
});

export default productsReducer;
