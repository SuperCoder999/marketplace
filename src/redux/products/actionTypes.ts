enum ProductActions {
	LoadProducts = "products:load",
	LoadMoreProducts = "products:load:more",
	LoadExpanded = "products:expanded:load",
	RemoveExpanded = "products:expnaded:remove",
	Create = "products:create",
	Update = "products:update",
	Delete = "products:delete",
}

export default ProductActions;
