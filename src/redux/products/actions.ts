import { createAction, createAsyncThunk } from "@reduxjs/toolkit";
import { Asset } from "react-native-image-picker";

import {
	getProduct,
	getProducts,
	postProduct,
	putProduct,
	removeProduct,
} from "../../services/product.service";

import EditProductDto from "../../typing/dto/product/edit-product.dto";
import ProductFilterDto from "../../typing/dto/product/product-filter.dto";
import ProductListDto from "../../typing/dto/product/product-list.dto";
import ProductSavedDto from "../../typing/dto/product/product-saved.dto";
import ProductDto from "../../typing/dto/product/product.dto";
import RootState from "../state";
import ProductActions from "./actionTypes";

export const loadProducts = createAsyncThunk<
	{ filter: ProductFilterDto; products: ProductListDto[] },
	Partial<ProductFilterDto> | undefined
>(ProductActions.LoadProducts, async (newFilter, { getState }) => {
	const state = getState() as RootState;
	const filter = state.products.productsPagination;
	const composedFilter = newFilter ? { ...filter, ...newFilter } : filter;

	return {
		filter: composedFilter,
		products: await getProducts(composedFilter),
	};
});

export const loadMoreProducts = createAsyncThunk<ProductListDto[]>(
	ProductActions.LoadMoreProducts,
	(_, { getState }) => {
		const state = getState() as RootState;
		const existingPagination = state.products.productsPagination;

		const pagination: ProductFilterDto = {
			...existingPagination,
			page: existingPagination.page + 1,
		};

		return getProducts(pagination);
	}
);

export const loadExpandedProduct = createAsyncThunk<ProductDto | null, number>(
	ProductActions.LoadExpanded,
	(id) => getProduct(id)
);

export const removeExpandedProduct = createAction<void>(
	ProductActions.RemoveExpanded
);

export const createProduct = createAsyncThunk<
	ProductSavedDto | null,
	{ data: EditProductDto; pictures: Asset[] }
>(ProductActions.Create, ({ data, pictures }) => postProduct(data, pictures));

export const updateProduct = createAsyncThunk<
	ProductSavedDto | null,
	{
		data: EditProductDto;
		pictures: Asset[];
		deletedPictureIds: number[];
	}
>(
	ProductActions.Update,
	({ data, pictures, deletedPictureIds }, { getState }) => {
		const state = getState() as RootState;
		const id = state.products.expandedProduct?.id;

		if (!id) {
			return null;
		}

		return putProduct(id, data, pictures, deletedPictureIds);
	}
);

export const deleteProduct = createAsyncThunk<number | null>(
	ProductActions.Delete,
	async (_, { getState }) => {
		const state = getState() as RootState;
		const id = state.products.expandedProduct?.id;

		if (!id) {
			return null;
		}

		await removeProduct(id);
		return id;
	}
);
