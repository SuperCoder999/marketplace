import { createReducer, isAnyOf } from "@reduxjs/toolkit";
import { loadUser, login, logout, register } from "./actions";
import AuthState, { initialState } from "./state";

const authReducer = createReducer<AuthState>(initialState, (builder) => {
	builder.addCase(logout.fulfilled, (state) => ({
		...state,
		user: null,
		userLoaded: true,
		loading: false,
	}));

	builder.addMatcher(
		isAnyOf(
			login.pending,
			loadUser.pending,
			register.pending,
			logout.pending
		),
		(state) => ({
			...state,
			loading: true,
		})
	);

	builder.addMatcher(
		isAnyOf(login.fulfilled, loadUser.fulfilled, register.fulfilled),
		(state, { payload: user }) => ({
			...state,
			user,
			userLoaded: true,
			loading: false,
		})
	);
});

export default authReducer;
