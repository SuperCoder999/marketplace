import UserDto from "../../typing/dto/user/user.dto";

export default interface AuthState {
	user: UserDto | null;
	userLoaded: boolean;
	loading: boolean;
}

export const initialState: AuthState = {
	user: null,
	userLoaded: false,
	loading: false,
};
