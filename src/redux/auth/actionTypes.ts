enum AuthActions {
	Login = "user:auth:login",
	LoadUser = "user:load",
	Register = "user:auth:register",
	Logout = "user:auth:logout",
}

export default AuthActions;
