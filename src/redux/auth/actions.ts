import { createAsyncThunk } from "@reduxjs/toolkit";
import { Asset } from "react-native-image-picker";
import UserCredentialsDto from "../../typing/dto/user/user-credentials.dto";
import UserRegisterDto from "../../typing/dto/user/user-register.dto";
import UserDto from "../../typing/dto/user/user.dto";

import {
	getUser,
	loginUser,
	registerUser,
	logoutUser,
} from "../../services/user.service";

import AuthActions from "./actionTypes";

export const login = createAsyncThunk<UserDto | null, UserCredentialsDto>(
	AuthActions.Login,
	(data) => loginUser(data)
);

export const loadUser = createAsyncThunk<UserDto | null>(
	AuthActions.LoadUser,
	() => getUser()
);

export const register = createAsyncThunk<
	UserDto | null,
	{ data: UserRegisterDto; avatar: Asset | null }
>(AuthActions.Register, ({ data, avatar }) => registerUser(data, avatar));

export const logout = createAsyncThunk<void>(AuthActions.Logout, () =>
	logoutUser()
);
