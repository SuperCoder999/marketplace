import { ReducerWithInitialState } from "@reduxjs/toolkit/dist/createReducer";
import authReducer from "./auth/reducer";
import productsReducer from "./products/reducer";
import RootState from "./state";

const rootReducer: Record<keyof RootState, ReducerWithInitialState<any>> = {
	auth: authReducer,
	products: productsReducer,
};

export default rootReducer;
