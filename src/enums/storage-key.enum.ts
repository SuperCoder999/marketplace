enum StorageKey {
	UserToken = "user:auth:token",
}

export default StorageKey;
