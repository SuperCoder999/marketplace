export enum Color {
	White = "#fafafa",
	Modal = "#fff8",
	GreyLight = "#ccc",
	GreyDark = "#666",
	Black = "#212121",
	Error = "#c75",
	Primary = "#59e",
	Secondary = "#6eb",
}

export default Color;
