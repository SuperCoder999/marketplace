export interface FormFile {
	uri: string;
	name: string;
	type: string;
}
