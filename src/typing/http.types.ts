import { Asset } from "react-native-image-picker";
import HttpMethod from "../enums/http-method.enum";

export interface RequestArgs {
	endpont: string;
	method?: HttpMethod;
	body?: Record<string, any>;
	query?: Record<string, string | number>;
	headers?: Record<string, string>;
	attachment?: Asset;
	attachmentFieldName?: string;
	alertError?: boolean;
}

export type BodyInit =
	| Blob
	| Int8Array
	| Int16Array
	| Int32Array
	| Uint8Array
	| Uint16Array
	| Uint32Array
	| Uint8ClampedArray
	| Float32Array
	| Float64Array
	| DataView
	| ArrayBuffer
	| FormData
	| string
	| null
	| undefined;
