export default interface SellerDto {
	name: string;
	email: string;
	avatar?: string;
	phoneNumber: string;
}
