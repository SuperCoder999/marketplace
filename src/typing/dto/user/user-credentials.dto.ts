export default interface UserCredentialsDto {
	email: string;
	password: string;
}
