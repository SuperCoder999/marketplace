export default interface UserDto {
	id: number;
	name: string;
	email: string;
	avatar?: string;
	phoneNumber: string;
}
