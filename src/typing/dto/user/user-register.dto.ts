export default interface UserRegisterDto {
	name: string;
	email: string;
	phoneNumber: string;
	password: string;
}
