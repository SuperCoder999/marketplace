export default interface AuthorizedUserDto {
	id: number;
	name: string;
	email: string;
	token: string;
	role: string;
}
