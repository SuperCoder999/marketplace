export default interface PictureDto {
	id: number;
	url: string;
}
