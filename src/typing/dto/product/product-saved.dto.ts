import PictureDto from "../picture/picture.dto";

export default interface ProductSavedDto {
	id: number;
	title: string;
	price: number;
	description: string;
	pictures: PictureDto[];
	createdAt: Date;
}
