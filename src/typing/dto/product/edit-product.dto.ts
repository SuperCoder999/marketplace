export default interface EditProductDto {
	title: string;
	price: number;
	description: string;
}
