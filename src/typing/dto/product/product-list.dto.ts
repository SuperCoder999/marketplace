export default interface ProductListDto {
	id: number;
	title: string;
	price: number;
	description: string;
	picture?: string;
}
