import PictureDto from "../picture/picture.dto";
import SellerDto from "../user/seller.dto";

export default interface ProductDto {
	id: number;
	title: string;
	price: number;
	description: string;
	pictures: PictureDto[];
	seller: SellerDto;
	createdAt: Date;
}
