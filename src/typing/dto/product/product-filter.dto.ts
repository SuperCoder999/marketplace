export default interface ProductFilterDto {
	page: number;
	filter: string;
}
