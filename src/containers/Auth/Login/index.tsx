import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Button, View } from "react-native";
import LoginForm from "../../../components/Form/LoginForm";
import Color from "../../../enums/color.enum";
import { login } from "../../../redux/auth/actions";
import { usePromiseDispatch } from "../../../redux/store";
import UserCredentialsDto from "../../../typing/dto/user/user-credentials.dto";
import styles from "../styles";

const Login: React.FC = () => {
	const navigation = useNavigation();
	const dispatch = usePromiseDispatch();

	const submit = (data: UserCredentialsDto) => {
		dispatch(login(data));
	};

	return (
		<View style={styles.sheet}>
			<LoginForm onSubmit={submit} />
			<View style={styles.switchButton}>
				<Button
					title="or Register"
					color={Color.GreyDark}
					onPress={() => navigation.navigate("Register" as never)}
				/>
			</View>
		</View>
	);
};

export default Login;
