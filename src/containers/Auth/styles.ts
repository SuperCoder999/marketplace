import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	outerSheet: {
		flexGrow: 1,
		width: "100%",
	},
	container: {
		flexGrow: 1,
		justifyContent: "center",
	},
	sheet: {
		paddingHorizontal: 20,
		justifyContent: "center",
		alignItems: "center",
		flexGrow: 1,
		width: "100%",
	},
	switchButton: {
		marginTop: 25,
	},
});

export default styles;
