import React from "react";
import { Button, ScrollView, View } from "react-native";
import { Asset } from "react-native-image-picker";
import { useNavigation } from "@react-navigation/native";
import RegisterForm from "../../../components/Form/RegisterForm";
import Color from "../../../enums/color.enum";
import { register } from "../../../redux/auth/actions";
import { usePromiseDispatch } from "../../../redux/store";
import UserRegisterDto from "../../../typing/dto/user/user-register.dto";
import styles from "../styles";

const Register: React.FC = () => {
	const navigation = useNavigation();
	const dispatch = usePromiseDispatch();

	const submit = (data: UserRegisterDto, avatar: Asset | null) => {
		dispatch(register({ data, avatar }));
	};

	return (
		<ScrollView
			style={styles.outerSheet}
			contentContainerStyle={styles.container}
		>
			<View style={styles.sheet}>
				<RegisterForm onSubmit={submit} />
				<View style={styles.switchButton}>
					<Button
						title="or Login"
						color={Color.GreyDark}
						onPress={() => navigation.goBack()}
					/>
				</View>
			</View>
		</ScrollView>
	);
};

export default Register;
