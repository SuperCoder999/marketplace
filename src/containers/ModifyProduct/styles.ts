import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	scroller: {
		flexGrow: 1,
		width: "100%",
	},
	container: {
		width: "100%",
		padding: 20,
	},
});

export default styles;
