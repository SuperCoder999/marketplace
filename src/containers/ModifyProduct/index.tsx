import React, { useEffect } from "react";
import { ScrollView } from "react-native";
import { useSelector } from "react-redux";
import { Asset } from "react-native-image-picker";
import { useNavigation } from "@react-navigation/native";
import CreateProductForm from "../../components/Form/CreateProductForm";
import EditProductForm from "../../components/Form/EditProductForm";
import Preloader from "../../components/Preloader";
import EditProductDto from "../../typing/dto/product/edit-product.dto";
import { usePromiseDispatch } from "../../redux/store";
import RootState from "../../redux/state";

import {
	createProduct,
	updateProduct,
	deleteProduct,
} from "../../redux/products/actions";

import styles from "./styles";

const ModifyProduct: React.FC = () => {
	const navigation = useNavigation();
	const dispatch = usePromiseDispatch();

	const { loading, expandedProduct } = useSelector(
		(state: RootState) => state.products
	);

	useEffect(() => {
		if (!expandedProduct) {
			navigation.navigate("List" as never);
		}
	}, [expandedProduct]);

	const submitCreate = (data: EditProductDto, pictures: Asset[]) => {
		dispatch(createProduct({ data, pictures }));
	};

	const submitUpdate = (
		data: EditProductDto,
		pictures: Asset[],
		deletedPictureIds: number[]
	) => {
		dispatch(updateProduct({ data, pictures, deletedPictureIds }));
	};

	const submitDelete = () => {
		dispatch(deleteProduct());
	};

	if (loading) {
		return <Preloader />;
	}

	return (
		<ScrollView
			style={styles.scroller}
			contentContainerStyle={styles.container}
		>
			{expandedProduct ? (
				<EditProductForm
					defaultProduct={expandedProduct}
					onSubmit={submitUpdate}
					onSubmitDelete={submitDelete}
				/>
			) : (
				<CreateProductForm onSubmit={submitCreate} />
			)}
		</ScrollView>
	);
};

export default ModifyProduct;
