import { StyleSheet } from "react-native";
import Color from "../../enums/color.enum";

const styles = StyleSheet.create({
	list: {
		paddingHorizontal: 20,
	},
	header: {
		paddingTop: 10,
		paddingHorizontal: 20,
		alignItems: "center",
		backgroundColor: Color.White,
	},
});

export default styles;
