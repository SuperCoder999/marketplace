import React, { useEffect, useState } from "react";
import { FlatList, Text, View } from "react-native";
import { useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import ProductListDto from "../../typing/dto/product/product-list.dto";
import Preloader from "../../components/Preloader";

import {
	loadExpandedProduct,
	loadMoreProducts,
	loadProducts,
} from "../../redux/products/actions";

import RootState from "../../redux/state";
import { usePromiseDispatch } from "../../redux/store";
import Product from "../../components/Product";
import styles from "./styles";
import Input from "../../components/Input";
import { logout } from "../../redux/auth/actions";

const ProductList: React.FC = () => {
	const navigation = useNavigation();
	const dispatch = usePromiseDispatch();

	const {
		products,
		productsLoaded,
		expandedProduct,
		expandedProductLoaded,
		productsPagination: { filter },
		hasMoreProducts,
		loading,
		user,
	} = useSelector((state: RootState) => ({
		...state.products,
		user: state.auth.user,
	}));

	const [search, setSearch] = useState<string>(filter ?? "");

	useEffect(() => {
		if (!productsLoaded) {
			dispatch(loadProducts());
		}
	}, [dispatch, productsLoaded]);

	useEffect(() => {
		setSearch(filter ?? "");
	}, [filter]);

	useEffect(() => {
		if (expandedProductLoaded && expandedProduct) {
			const navigationScreen =
				user?.email === expandedProduct.seller.email // We can't compare by id :(
					? "Edit"
					: "ListExpanded";

			navigation.navigate(navigationScreen as never);
		}
	}, [expandedProductLoaded, expandedProduct]);

	const loadMore = () => {
		if (hasMoreProducts) {
			dispatch(loadMoreProducts());
		}
	};

	const reload = () => {
		dispatch(loadProducts({ filter: search }));
	};

	const expand = (id: number) => {
		dispatch(loadExpandedProduct(id));
	};

	const dispatchLogout = () => {
		dispatch(logout());
	};

	return (
		<>
			{loading ? <Preloader /> : null}

			<FlatList<ProductListDto>
				style={styles.list}
				data={products}
				onEndReachedThreshold={0.1}
				stickyHeaderIndices={[0]}
				keyExtractor={(product) => String(product.id)}
				renderItem={(item) => (
					<Product
						product={item.item}
						onExpand={() => expand(item.item.id)}
					/>
				)}
				ListHeaderComponent={
					<View style={styles.header}>
						<Text onPress={dispatchLogout}>Log out</Text>
						<Input
							value={search}
							placeholder="Search..."
							icon="search-circle"
							onChange={setSearch}
							onIconPress={reload}
						/>
					</View>
				}
				onEndReached={loadMore}
			/>
		</>
	);
};

export default ProductList;
