import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	root: {
		flexGrow: 1,
	},
});

export default styles;
