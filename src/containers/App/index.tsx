import React from "react";
import { SafeAreaView } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { Provider as ReduxProvider } from "react-redux";
import RootNavigator from "../../navigators/RootNavigator";
import store from "../../redux/store";
import styles from "./styles";

const App: React.FC = () => {
	return (
		<SafeAreaView style={styles.root}>
			<ReduxProvider store={store}>
				<NavigationContainer>
					<RootNavigator />
				</NavigationContainer>
			</ReduxProvider>
		</SafeAreaView>
	);
};

export default App;
