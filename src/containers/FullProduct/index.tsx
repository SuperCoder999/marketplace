import React, { useEffect } from "react";
import { View, Text, Image, ScrollView, Button } from "react-native";
import { useSelector } from "react-redux";
import { useNavigation } from "@react-navigation/native";
import RootState from "../../redux/state";
import ImageCarousel from "../../components/ImageCarousel";
import { DEFAULT_PICTURE } from "../../constants/picture.constants";
import Color from "../../enums/color.enum";
import { makeCall } from "../../helpers/native.helper";
import styles from "./styles";

const FullProduct: React.FC = () => {
	const navigation = useNavigation();

	const product = useSelector(
		(state: RootState) => state.products.expandedProduct
	);

	useEffect(() => {
		if (!product) {
			navigation.navigate("List" as never);
		}
	}, [product]);

	if (!product) {
		return null;
	}

	const { title, description, price, pictures, seller } = product;

	return (
		<ScrollView style={styles.scroller}>
			<View style={styles.container}>
				<ImageCarousel urls={pictures.map((p) => p.url)} />
				<View style={styles.spaceBetween}>
					<Text style={styles.black}>{title}</Text>
					<Text style={styles.black}>{price}$</Text>
				</View>
				<Text>{description}</Text>
				<View style={styles.seller}>
					<Image
						source={{ uri: seller.avatar ?? DEFAULT_PICTURE }}
						style={styles.avatar}
					/>
					<View>
						<Text>{seller.name}</Text>
						<Text>{seller.phoneNumber}</Text>
					</View>
				</View>
				<View style={styles.spaceBetween}>
					<Button
						title="Call seller"
						color={Color.Secondary}
						onPress={() => makeCall(seller.phoneNumber)}
					/>
					<Button
						title="Back"
						color={Color.GreyDark}
						onPress={() => navigation.goBack()}
					/>
				</View>
			</View>
		</ScrollView>
	);
};

export default FullProduct;
