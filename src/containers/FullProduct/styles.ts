import { StyleSheet } from "react-native";
import Color from "../../enums/color.enum";

const styles = StyleSheet.create({
	scroller: {
		flexGrow: 1,
		width: "100%",
	},
	container: {
		width: "100%",
		padding: 20,
	},
	spaceBetween: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	black: {
		color: Color.Black,
		fontSize: 24,
	},
	seller: {
		flexDirection: "row",
		marginVertical: 10,
	},
	avatar: {
		width: 40,
		height: 40,
		marginRight: 10,
	},
});

export default styles;
